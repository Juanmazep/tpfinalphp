<?php

class Pelicula_Controller {

  var $messages = null;
  
  function getAllPeliculasFull(){
    $var_clase = new Pelicula_Model();
    $result = $var_clase->getAllPeliculasFull();
    if ($result){
        return $result;
    }
    else{
        return false;
    }
  }
  
  function getAllGeneros(){
      $var_model = new Pelicula_Model();
      $result = $var_model->getAllGeneros();
      if ($result){
          return $result;
      }
      else{
          return false;
      }
  }
  
  function listadoGeneros($tpl, $elegido = "ninguno"){
  /*  $tpl = new TemplatePower("templates/altaPelicula.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");
    */
   
    $pelicula_model = new Pelicula_Model();
    $resultGeneros = $pelicula_model->getAllGeneros();
    if ($resultGeneros){
        foreach ($resultGeneros as $genero){
          $tpl->newBlock("block_listado_generos");
          $tpl->assign("var_genero_id", $genero["id_genero"]);
          $tpl->assign("var_genero_nombre", $genero["ge_nombre"]);
          if ($elegido == $genero["ge_nombre"]){
              $tpl->assign("var_genero_selected", "selected");
          }
        }

    }
   //return $tpl->getOutputContent();
  }
  
  function listadoDirectores($tpl, $elegido = "ninguno"){
    $pelicula_model = new Pelicula_Model();
    $resultDirectores = $pelicula_model->getAllDirectores();
    if ($resultDirectores){
        foreach ($resultDirectores as $director){
            $tpl->newBlock("block_listado_directores");
            $tpl->assign("var_director_id", $director["id_director"]);
            $tpl->assign("var_director_nombre", $director["di_nombreArtistico"]);
            if ($elegido == $director["di_nombreArtistico"]){
                $tpl->assign("var_director_selected", "selected");
            }
        }
    }
  }

  function listadoDirectores2(){
    $tpl = new TemplatePower("templates/listadoDirectores.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");
    $pelicula_model = new Pelicula_Model();
    $resultDirectores = $pelicula_model->getAllDirectoresInfo();
    if ($resultDirectores){
        foreach($resultDirectores as $director){
            $tpl->newBlock("block_listado_directores");
            $tpl->assign("var_director_nombre", $director["ar_nombre"]);
            $tpl->assign("var_director_apellido", $director["ar_apellido"]);
            $tpl->assign("var_director_nombre_artistico", $director["di_nombreArtistico"]);
            $tpl->assign("var_director_dni", $director["ar_dni"]);
            $tpl->assign("var_director_mail", $director["ar_mail"]);
        }   
    }else{
        $tpl->assign("noresults_directores");
    }
    $tpl->gotoBlock("_ROOT");

    return $tpl->getOutputContent();
    }
  


  function listadoPeliculas(){
    $tpl = new TemplatePower("templates/listadoPeliculas.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");
    
    $pelicula_model = new Pelicula_Model();
    $resultPeliculas = $pelicula_model->getAllPeliculasFull();
    if ($resultPeliculas){
        foreach ($resultPeliculas as $pelicula){
            $tpl->newBlock("block_listado_peliculas");
            $tpl->assign("var_pelicula_nombre", $pelicula["pe_nombre"]);
            $tpl->assign("var_pelicula_genero", $pelicula["ge_nombre"]);
            $tpl->assign("var_pelicula_director", $pelicula["di_nombreArtistico"]);
            $tpl->assign("var_pelicula_duracion", $pelicula["pe_duracion"]. ' minutos');    
            $actores = $this->actoresPelicula($pelicula["id_pelicula"]);             
            $tpl->assign("var_pelicula_actores", $actores);
            $tpl->assign("idPelicula", $pelicula["id_pelicula"]);
        }
    }else{
        $tpl->assign("noresults_peliculas");
    }
    $tpl->gotoBlock("_ROOT");

    return $tpl->getOutputContent();
  }

  function listadoPeliculasPorGenero(){
    $tpl = new TemplatePower("templates/listadoPeliculasPorGenero.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");  
    $pelicula_model = new Pelicula_Model();
    $resultPeliculas = $pelicula_model->peliculasPorGenero();
    $generoActual = "";
    if ($resultPeliculas){
        foreach ($resultPeliculas as $pelicula){
            if ($pelicula["ge_nombre"] != $generoActual){
                $generoActual = $pelicula["ge_nombre"];
                $tpl->newBlock("block_genero");
                $tpl->assign("var_genero", $pelicula["ge_nombre"]);
            }
            $tpl->newBlock("block_listado_peliculas");
            $tpl->assign("var_pelicula_nombre", $pelicula["pe_nombre"]);
            $tpl->assign("var_pelicula_director", $pelicula["di_nombreArtistico"]);
            $tpl->assign("var_pelicula_duracion", $pelicula["pe_duracion"] . " minutos");
            $actores = $this->actoresPelicula($pelicula["id_pelicula"]);   
            $tpl->assign("var_pelicula_actores", $actores);
        }
    }else{
        $tpl->assign("noresults_peliculas");
    }
    $tpl->gotoBlock("_ROOT");

    return $tpl->getOutputContent();
  }

  function listadoPeliculasPorDirector(){
    $tpl = new TemplatePower("templates/listadoPeliculasPorDirector.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");  
    $pelicula_model = new Pelicula_Model();
    $resultPeliculas = $pelicula_model->peliculasPorDirector();
    $directorActual = "";
    if ($resultPeliculas){
        foreach($resultPeliculas as $pelicula){
            if($pelicula["di_nombreArtistico"] != $directorActual){
                $directorActual = $pelicula["di_nombreArtistico"];
                $tpl->newBlock("block_director");
                $tpl->assign("var_director", $pelicula["di_nombreArtistico"]);
            }
            $tpl->newBlock("block_listado_peliculas");
            $tpl->assign("var_pelicula_nombre", $pelicula["pe_nombre"]);
            $tpl->assign("var_pelicula_genero", $pelicula["ge_nombre"]);
            $tpl->assign("var_pelicula_duracion", $pelicula["pe_duracion"] . " minutos");
            $actores = $this->actoresPelicula($pelicula["id_pelicula"]);   
            $tpl->assign("var_pelicula_actores", $actores);
        }
    }else{
        $tpl->assign("noresults_peliculas");
    }
    $tpl->gotoBlock("_ROOT");
    return $tpl->getOutputContent();
  }
  
  function altaPelicula() {
    $tpl = new TemplatePower("templates/altaPelicula.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");
    /*
    $pelicula_model = new Pelicula_Model();
    $resultGeneros = $pelicula_model->getAllGeneros();
    if ($resultGeneros){
        foreach ($resultGeneros as $genero){
          $tpl->newBlock("block_listado_generos");
          $tpl->assign("var_genero_nombre", $genero["ge_nombre"]);
        }

    }

     */
    $this->listadoGeneros($tpl);
    $this->listadoDirectores($tpl);
    return $tpl->getOutputContent();
  }
  
  function agregarPelicula() {
      
    $idDirector = $_REQUEST['director'];
    $idGenero = $_REQUEST['genero'];
    $peNombre = $_REQUEST['nombrePelicula'];
    $peDuracion = $_REQUEST['duracion'];
    $peFechaEstreno = $_REQUEST['fechaEstreno'];
    /*   
    $idDirector = 2;
    $idGenero = 2;
    $peNombre = 'prueba';
    $peDuracion = 120;
    $peFechaEstreno = '11/12/1222';
    */
    $pelicula_model = new Pelicula_Model();
    $result = $pelicula_model->altaPelicula($idDirector, $idGenero, $peNombre, $peDuracion, $peFechaEstreno);
    
    
    if ($result){
        $tpl = new TemplatePower("templates/altaPelicula.html");
        $tpl->prepare();
        $tpl->gotoBlock("_ROOT");
        $tpl->newBlock("block_valor_submit");
        $tpl->assign("var_valor_submit", "<b style='color: green'>Película dada de alta exitosamente!</b>");
        $this->listadoGeneros($tpl);
        $this->listadoDirectores($tpl);
        return $tpl->getOutputContent();
    }else{
        $tpl = new TemplatePower("templates/altaPelicula.html");
        $tpl->prepare();
        $tpl->gotoBlock("_ROOT");
        $tpl->newBlock("block_valor_submit");
        $tpl->assign("var_valor_submit", "<b style='color: red'>Error, no se pudo dar alta a la película. Intente nuevamente</b>");
        $this->listadoGeneros($tpl);
        $this->listadoDirectores($tpl);
        return $tpl->getOutputContent();
    }
   
  }

  function editarPelicula(){
    $idPelicula = $_GET['id'];
    $tpl = new TemplatePower("templates/editarPelicula.html");
    $tpl->prepare();
    $tpl->gotoBlock("_ROOT");
    $pelicula_model = new Pelicula_Model();
    $pelicula = $pelicula_model->getSinglePelicula($idPelicula)[0];
    if ($pelicula){
        //var_dump($pelicula);
        //echo $pelicula["pe_nombre"];
        $tpl->newBlock("block_pelicula_datos");
        $tpl->assign("var_pelicula_nombre", $pelicula["pe_nombre"]);  
        $tpl->assign("var_genero_nombre", $pelicula["ge_nombre"]);
        $tpl->assign("var_director_nombre", $pelicula["di_nombreArtistico"]);
        $tpl->assign("var_pelicula_duracion", $pelicula["pe_duracion"]);
        $tpl->assign("var_pelicula_fecha_estreno", $pelicula["pe_fechaEstreno"]);
        $tpl->assign("var_genero_id", $pelicula["id_genero"]);
        $this->listadoGeneros($tpl, $pelicula["ge_nombre"]);
        $this->listadoDirectores($tpl, $pelicula["di_nombreArtistico"]);
        
        
    }else{
        echo "error";
    }
    return $tpl->getOutputContent();
  }

  function editarPeliculaEnDb(){
    $idPelicula = $_REQUEST['id'];
    $idDirector = $_REQUEST['director'];
    $idGenero = $_REQUEST['genero'];
    $peNombre = $_REQUEST['nombrePelicula'];
    $peDuracion = $_REQUEST['duracion'];
    $peFechaEstreno = $_REQUEST['fechaEstreno'];
    $pelicula_model = new Pelicula_Model();
    //$result = $pelicula_model->editarPelicula($peNombre, $idGenero, $idDirector, $peDuracion, $peFechaEstreno, $idPelicula);
    $result = $pelicula_model->editarPelicula($peNombre, $idGenero, $idDirector, $peDuracion, $peFechaEstreno, $idPelicula);    
    if ($result){
        header('Location: index.php?action=Pelicula::listadoPeliculas');
    }else{
        echo 'faaaaaail';
        echo $idPelicula . "<br>";
        echo $idPelicula . " " . $idGenero . " " . $idDirector . " " . $peNombre . " " . $peDuracion . " " . $peFechaEstreno;
    }
  }

  function eliminarPelicula(){
    $idPelicula = $_GET['id'];
    $pelicula_model = new Pelicula_Model();
    $result = $pelicula_model->eliminarPelicula($idPelicula);
    if ($result){
        header('Location: index.php?action=Pelicula::listadoPeliculas');
    }else{
        echo 'fallo al borrar';
    }
  }
  
    function actoresPelicula($idPelicula){
        $var_clase = new Pelicula_Model();
        $result = $var_clase->actoresPelicula($idPelicula);
        if ($result){
            $actorString = '';
            foreach ($result as $actor){
                $actorString .= $actor['ac_nombreArtistico'] . ', '; 
            }
            $actorString = trim($actorString, ", ");
            return $actorString;
        }else{
            return 'no hay actores en db';
        }
    }
    
  
  /*

    function getAllPeliculas(){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->getAllPeliculas();
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }

        function getAllPeliculasFull(){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->getAllPeliculasFull();
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }
        
        function listarPeliculas(){
            $tpl = new TemplatePower("templates/listadoPeliculas.html");
            $tpl->prepare();
            $tpl->gotoBlock("_ROOT");
            $pelicula_model = new Pelicula_Model();
            $resultPeliculas = $pelicula_model->getAllPeliculasFull();
            if ($resultPeliculas){
                foreach ($resultPeliculas as $pelicula){
                    $tpl->newBlock("block_listado_peliculas");
                    $tpl->assign("var_pelicula_nombre", $pelicula["pe_nombre"]);
                    $tpl->assign("var_pelicula_genero", $pelicula["ge_nombre"]);
                    $tpl->assign("var_pelicula_director", $pelicula["di_nombreArtistico"]);
                    $tpl->assign("var_pelicula_duracion", $pelicula["pe_duracion"]. ' minutos');    
                    $actores = $this->actoresPelicula($pelicula["id_pelicula"]);             
                    $tpl->assign("var_pelicula_actores", $actores);
                }
            }else{
                $tpl->assign("noresults_peliculas");
            }
            
            return $tpl->getOutputContent();
            
        }
        
        function actoresPelicula($idPelicula){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->actoresPelicula($idPelicula);
            if ($result){
                $actorString = '';
                foreach ($result as $actor){
                    $actorString .= $actor['ac_nombreArtistico'] . ', '; 
                }
                $actorString = trim($actorString, ", ");
                return $actorString;
            }else{
                return false;
            }
        }

        function peliculasPorGenero(){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->peliculasPorGenero();
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }

        function peliculasPorDirector(){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->peliculasPorDirector();
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }

        function cambiarGenero($nombrePelicula, $generoActual, $generoNuevo){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->cambiarGenero($nombrePelicula, $generoActual, $generoNuevo);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }

        function eliminarActoresPelicula($idPelicula){
            $var_clase = new Pelicula_Model();
            $result = $var_clase->eliminarActoresPelicula($idPelicula);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }  
*/
 
}
