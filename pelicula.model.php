<?php
    class Pelicula_Model {
        
        function getAllPeliculas(){
            global $db;
            $sql = "SELECT * FROM pelicula";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }
        
        function getAllGeneros(){
            global $db;
            $sql = "SELECT * FROM genero";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }
        
        function getAllDirectores(){
           global $db;
           $sql = "SELECT * FROM director";
           $result = $db->select($sql);
           if ($result){
               return $result;
           }
           else{
               return false;
           }
        }

        function getAllDirectoresInfo(){
            global $db;
            $sql = "SELECT a.ar_nombre, a.ar_apellido, a.ar_dni, a.ar_mail , d.di_nombreArtistico FROM artista a, director d WHERE a.id_artista = d.id_artista";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }else{
                return false;
            }
        }

        function getAllPeliculasFull(){
            global $db;
            $sql = "SELECT p.id_pelicula, p.pe_nombre, p.pe_duracion, g.ge_nombre, d.di_nombreArtistico FROM pelicula p, genero g, director d WHERE p.id_genero = g.id_genero AND p.id_director = d.id_director ORDER BY p.id_pelicula";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }

        function getSinglePelicula($idPelicula){
            global $db;
            $sql = "SELECT p.id_pelicula, p.pe_nombre, p.pe_duracion, p.pe_fechaEstreno, g.ge_nombre, d.di_nombreArtistico, p.id_genero, p.id_director FROM pelicula p, genero g, director d WHERE id_pelicula = $idPelicula AND p.id_genero = g.id_genero AND p.id_director = d.id_director";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }else{
                return false;
            }
        }
        
        function actoresPelicula($idPelicula){
            global $db;
            $sql = "SELECT a.ac_nombreArtistico from actor a, pelicula_actor p where  a.id_actor = p.id_actor and p.id_pelicula = '$idPelicula'";
            $result = $db->select($sql);
            if($result){
                return $result;
            }else{
                return false;
            }
            
        }
    
        function insertarActor($nombre, $apellido, $dni, $mail, $nombreArtistico) {
    
        }
    
        function peliculasPorGenero() {
            global $db;
            $sql = "SELECT p.pe_nombre, p.id_pelicula, p.pe_duracion, d.di_nombreArtistico, g.ge_nombre FROM pelicula p, director d, genero g WHERE p.id_genero = g.id_genero AND p.id_director = d.id_director ORDER BY g.id_genero";
            $result = $db->select($sql);
            if ($result){
                return $result;
            }
            else{
                return false;
            }
        }
    
        function peliculasPorDirector() {
            global $db;
            $sql = "SELECT p.pe_nombre, p.id_pelicula, p.pe_duracion, d.di_nombreArtistico, g.ge_nombre FROM pelicula p, director d, genero g WHERE p.id_director = d.id_director AND p.id_genero = g.id_genero ORDER BY d.id_director";       
            $result = $db->select($sql);
            if ($result){
                return $result;
            }
            else {
                return false;
            }
        }
    
        function eliminarPelicula($idPelicula){
            global $db;
            $sql = "DELETE FROM pelicula_actor WHERE id_pelicula = $idPelicula";
            $db->delete($sql);
            $sql = "DELETE FROM pelicula_sala WHERE id_pelicula = $idPelicula";
            $db->delete($sql);
            $sql = "DELETE FROM pelicula WHERE id_pelicula = $idPelicula";
            $result = $db->delete($sql);
            if ($result){
                return $result;
            }else{
                return false;
            }
        }
    
        function cambiarGenero($nombrePelicula, $generoActual, $generoNuevo) {
            global $db;
            $sqlFindIds = "SELECT g1.id_genero AS id_genero1, g2.id_genero AS id_genero2 from genero g1, genero g2 where g1.ge_nombre = '$generoActual' AND g2.ge_nombre = '$generoNuevo'";
            $result = $db->select($sqlFindIds);
            var_dump($result);
            $idGeneroActual = $result[0]['id_genero1'];
            $idGeneroNuevo = $result[0]['id_genero2'];
            $sql = "UPDATE pelicula SET id_genero = $idGeneroNuevo WHERE pe_nombre = '$nombrePelicula'";
            $result = $db->update($sql);
            return $result;
        }
    
        function eliminarActoresPelicula($idPelicula){
            global $db;
            $sql = "DELETE FROM pelicula_actor WHERE id_pelicula = $idPelicula";
            $result = $db->delete($sql);
            var_dump($result);
            return $result;
        }
        
        function altaPelicula($idDirector, $idGenero, $peNombre, $peDuracion, $peFechaEstreno){
            global $db;
            $sql = "INSERT INTO pelicula (id_director, id_genero, pe_nombre, pe_duracion, pe_fechaEstreno) VALUES ($idDirector, $idGenero, '$peNombre', $peDuracion, $peFechaEstreno)";
            $result = $db->query($sql);
            return $result;
        }

        function editarPelicula($peNombre, $idGenero, $idDirector, $peDuracion, $peFechaEstreno, $idPelicula){
            global $db;
            $sql = "UPDATE pelicula SET pe_nombre = '$peNombre', id_genero = $idGenero, id_director = $idDirector, pe_duracion = $peDuracion, pe_fechaEstreno = '$peFechaEstreno' WHERE id_pelicula = $idPelicula";
            $result = $db->query($sql);
            return $result;
        }
    }